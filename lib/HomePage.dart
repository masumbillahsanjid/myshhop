import 'package:flutter/material.dart';
import 'package:myshop/provider/cart.dart';
import 'package:myshop/screen/cartScreen.dart';
import 'package:myshop/screen/profileScreen.dart';
import 'package:provider/provider.dart';

import 'screen/dashboard.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int current = 0;

  Widget callpage(int _current) {
    switch (_current) {
      case 0:
        return DashBoard();
      case 1:
        return CartScreen();
      case 2:
        return Profile();
    }
  }

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child:  Scaffold(
              body: callpage(current),
              bottomNavigationBar: BottomNavigationBar(
                  onTap: (value) {
                    setState(() {
                      current = value;
                    });
                  },
                  currentIndex: current,
                  items: [
                    BottomNavigationBarItem(
                        title: Text("Home"), icon: Icon(Icons.home)),
                    BottomNavigationBarItem(
                        title: Text("Request"), icon: Icon(Icons.repeat)),
                    BottomNavigationBarItem(
                        title: Text("Profile"),
                        icon: Icon(Icons.perm_contact_calendar)),
                  ]),
            ),
    );
  }
}
