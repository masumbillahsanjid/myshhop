import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Auth with ChangeNotifier {
  String _token;
  DateTime _expiryDate;
  String _userId;

  Future<void> signup(String email, String password) async {
    SharedPreferences setSp = await SharedPreferences.getInstance();
    var url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBxAIMXMad2wlS-ZKxuzskKQCEc41T751w";

    final response = await http.post(url,
        body: json.encode({
          "email": email,
          "password": password,
          "returnSecureToken": true,
        }));
    if (response.statusCode == 200) {
      var data = json.decode(response.body);

      setToken = data['idToken'];

      setSp.setString("email", email);
      setSp.setString("idToken", data['idToken']);
      setSp.setString("localId", data['localId']);
      notifyListeners();
    }

  }

  set setToken(value) {
    _token = value;
  }

  get getToken {
    print(_token);
    return _token;
  }

  Future<void> SignIn(String email, String password) async {

    SharedPreferences setSp = await SharedPreferences.getInstance();
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    var url =
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBxAIMXMad2wlS-ZKxuzskKQCEc41T751w";

    final response = await http.post(url,
        body: json.encode({
          "email": email,
          "password": password,
          "returnSecureToken": true,
        }));

    if (response.statusCode == 200) {
      var data = json.decode(response.body);

      setToken = data['idToken'];

      setSp.setString("email", email);
      setSp.setString("idToken", data['idToken']);
      setSp.setString("localId", data['localId']);
      notifyListeners();
    }


  }

  Future<void> checkdata() async {
    SharedPreferences getSp = await SharedPreferences.getInstance();
    var url =
        "https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyBxAIMXMad2wlS-ZKxuzskKQCEc41T751w}";
    final response =
        await http.post(url, body: {"idToken": getSp.get("idToken")});

    print(json.decode(response.body));
  }
}
