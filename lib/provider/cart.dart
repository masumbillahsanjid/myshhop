import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class CartItem {
  final String id;
  final String title;
  final String imageUrl;
  final int quantity;
  final List colors;
  final double price;

  CartItem({
    @required this.id,
    @required this.imageUrl,
    @required this.title,
    @required this.quantity,
    @required this.colors,
    @required this.price,
  });
}

class Cart with ChangeNotifier {
  int get itemCount {
    return _MyCarItem.length;
  }

  double get totalAmount {
    var total = 0.0;
    _MyCarItem.forEach((cartItem) {
      total += cartItem.price * cartItem.quantity;
    });
    return total;
  }

  Future<void> AddSever(String productID, String imageurl, double price,
      String title, List colors) async {
    SharedPreferences getSp = await SharedPreferences.getInstance();
    print(productID);

    if (_MyCarItem.where((data) => data.imageUrl == imageurl).toList().length ==
        0) {
      var url =
          "https://myshop-853e9.firebaseio.com/cart/${getSp.get("localId")}.json?auth=${getSp.get("idToken")}";

      await http.post(url,
          body: json.encode({
            "id": productID,
            "imageUrl": imageurl,
            "title": title,
            "price": price.toString(),
            "colors": colors,
            "quantity": 1.toString()
          }));
    } else {
      print(false);
    }
  }

  List<CartItem> _MyCarItem = [];

  List<CartItem> get MyCarItem {
    return [..._MyCarItem];
  }

  Future<void> fetchandretirve() async {
    SharedPreferences getSp = await SharedPreferences.getInstance();

    try {
      var url =
          "https://myshop-853e9.firebaseio.com/cart/${getSp.get("localId")}.json?auth=${getSp.get("idToken")}";

      var response = await http.get(url);
      final extractData = json.decode(response.body) as Map<String, dynamic>;
      final List<CartItem> loadedProduct = [];
      extractData.forEach((proid, prodData) {
        loadedProduct.add(CartItem(
            id: proid,
            colors: prodData['colors'],
            title: prodData['title'],
            price: double.parse(prodData['price']),
            imageUrl: prodData['imageUrl'],
            quantity: 1));
        _MyCarItem = loadedProduct;
        notifyListeners();
      });
    } catch (error) {
      print(error);
    }

    print(MyCarItem.length);
  }

  Future<void> removeList(int indexNumm, String ProductID) async {
    _MyCarItem.removeAt(indexNumm);
    SharedPreferences getSp = await SharedPreferences.getInstance();
    final url =
        "https://myshop-853e9.firebaseio.com/cart/${getSp.get("localId")}/$ProductID.json?auth=${getSp.get("idToken")}";
    http.delete(url);
    notifyListeners();
  }

  void clear() {
    _MyCarItem = [];
    notifyListeners();
  }
}
