import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:myshop/model/product.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myshop/provider/auth.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductProvider with ChangeNotifier {
  List<Product> _item = [];

  List<Product> get item {
    return [..._item];
  }

  void addproduct() {
//    _item.add(value);
    notifyListeners();
  }

  List<Product> Category(String cat) {

    var data = _item.where((prod) => prod.Category == cat).toList();

    print(data);
    return data;
  }

  Future<void> fetchAndSetProduct(BuildContext context) async {
    Provider.of<Auth>(context, listen: false).getToken;
    SharedPreferences getSp = await SharedPreferences.getInstance();

    var url =
        "https://myshop-853e9.firebaseio.com/products.json?auth=${getSp.get("idToken")}";
    try {
      final response = await http.get(url);
      final extractData = json.decode(response.body) as Map<String, dynamic>;
      final List<Product> loadedProduct = [];
      extractData.forEach((proid, prodData) {
        loadedProduct.add(Product(
          id: proid,
          Category: prodData['Category'],
          colors: prodData['colors'],
          title: prodData['title'],
          description: prodData['description'],
          price: prodData['price'],
          ImageUrl: prodData['ImageUrl'],
        ));
      });

      _item = loadedProduct;

      notifyListeners();

    } catch (error) {
      print('${error} r');
    }
  }

  Product productByid(String id) {
    return _item.firstWhere((data) => data.id == id);
  }
}
