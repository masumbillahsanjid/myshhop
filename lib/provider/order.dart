import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Order with ChangeNotifier {
  Future<void> addOrder(
      String image, String amount, String quantity, String Colors) async {
    SharedPreferences getSp = await SharedPreferences.getInstance();

    var url =
        "https://myshop-853e9.firebaseio.com/order.json?auth=${getSp.get("idToken")}";

    var response = await http.post(url,
        body: json.encode({
          "username": getSp.get("email"),
          "image": image,
          "quantity": quantity,
          "totalamount": amount,
          "colors": Colors,
          "status": false
        }));

    if (response.statusCode == 200) {
      print("donw");
    } else {
      print("error");
    }
  }
}
