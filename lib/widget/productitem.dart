import 'package:flutter/material.dart';
import 'package:myshop/provider/cart.dart';

import 'package:provider/provider.dart';

import '../model/product.dart';

class ProductItem extends StatelessWidget {
//  var category;
//  ProductItem(this.category);
  @override
  Widget build(BuildContext context) {
    final prItem = Provider.of<Product>(context, listen: false);
    final cart = Provider.of<Cart>(context, listen: false);
    return Column(
      children: <Widget>[
        Container(
          height: 80,
          width: 100,
          decoration: BoxDecoration(
              image: DecorationImage(image: NetworkImage(prItem.ImageUrl))),
        ),
        FlatButton(
            onPressed: () {
//
              cart.AddSever(prItem.id, prItem.ImageUrl, prItem.price,
                  prItem.title, prItem.colors);
              Scaffold.of(context).hideCurrentSnackBar();
              Scaffold.of(context)
                  .showSnackBar(SnackBar(content: Text("This product added your cart succesfully")));
              cart.fetchandretirve();
            },
            child: Text("Add To Cart"))
      ],
    );
  }
}
