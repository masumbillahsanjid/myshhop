import 'package:flutter/material.dart';
import 'package:myshop/provider/auth.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../HomePage.dart';

class Signup extends StatefulWidget {
  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  Color maincolor = Color.fromRGBO(1, 117, 216, 1.0);
  String _email, _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _isloading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(250, 251, 255, 1.0),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.10,
              ),
              Center(
                child: Text(
                  "REGISTRATION",
                  style: TextStyle(
                      color: maincolor,
                      fontSize: 35,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                "Plese provide all valid information",
                style: TextStyle(color: Colors.grey),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.10,
              ),
              Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.87,
                        child: TextFormField(
                          onSaved: (input) => _email = input,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.email),
                              labelText: 'Email',
                              border: OutlineInputBorder()),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.87,
                        child: TextFormField(
                          onSaved: (input) => _password = input,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.lock),
                              labelText: 'Password',
                              border: OutlineInputBorder()),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.87,
                        height: MediaQuery.of(context).size.width * 0.12,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: maincolor.withOpacity(0.4),
                                  offset: Offset(0, 0),
                                  blurRadius: 5,
                                  spreadRadius: 2)
                            ]),
                        child: _isloading
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                color: maincolor,
                                onPressed: () {
                                  signUp().then((onValue) {
                                    setState(() {
                                      _isloading = false;
                                    });
                                    nextPage();
                                  });
                                },
                                child: Text(
                                  "REGISTER",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      Text(
                        "Already Have Account?",
                        style: TextStyle(color: Colors.grey),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Login",
                          style: TextStyle(
                              color: maincolor, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ))
            ],
          ),
        ));
  }

  Future<void> nextPage() async {
    SharedPreferences getSP = await SharedPreferences.getInstance();
    var email = getSP.getString('email');
    if (email != null) {
      Route route = MaterialPageRoute(builder: (context) => HomePage());
      Navigator.push(context, route);
    }
  }

  Future<void> signUp() async {
    setState(() {
      _isloading = true;
    });
    SharedPreferences getSP = await SharedPreferences.getInstance();
    var email = getSP.getString('email');
    _formKey.currentState.save();
    await Provider.of<Auth>(context, listen: false)
        .signup(_email, _password)
        .catchError((value) {
      setState(() {
        _isloading = false;
      });
    });
  }
}
