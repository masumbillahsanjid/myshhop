import 'dart:math';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:myshop/provider/auth.dart';
import 'package:myshop/screen/SignUp.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../HomePage.dart';
import 'dashboard.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _email, _password;

  bool _isloading = false;
  Color maincolor = Color.fromRGBO(1, 117, 216, 1.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(250, 251, 255, 1.0),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.10,
              ),
              Center(
                  child: Text(
                "Welcome To",
                style: TextStyle(fontSize: 20),
              )),
              Text(
                "MyShop",
                style: TextStyle(
                    color: maincolor,
                    fontSize: 40,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                "This is a online shoping apps for you",
                style: TextStyle(color: Colors.grey),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.10,
              ),
              Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.87,
                        child: TextFormField(
                          onSaved: (input) => _email = input,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person_outline),
                              labelText: 'Username',
                              border: OutlineInputBorder()),
                          validator: (input) {
                            if (input.isEmpty) {
                              return 'Provide an email';
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.87,
                        child: TextFormField(
                          onSaved: (input) => _password = input,
                          obscureText: true,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person_outline),
                              labelText: 'Password',
                              border: OutlineInputBorder()),
                          validator: (input) {
                            if (input.length < 6) {
                              return 'Longer password please';
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.87,
                        height: MediaQuery.of(context).size.width * 0.12,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: maincolor.withOpacity(0.4),
                                  offset: Offset(0, 0),
                                  blurRadius: 5,
                                  spreadRadius: 2)
                            ]),
                        child: _isloading
                            ? Center(child: CircularProgressIndicator())
                            : RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                color: maincolor,
                                onPressed: () {
                                  signIn().then((onValue) {
                                    setState(() {
                                      _isloading = false;
                                    });
                                    nextPage();
                                  });

//                            signIn();
//                            Route route =
//                            MaterialPageRoute(builder: (context) => Test());
//                            Navigator.push(context, route);
                                },
                                child: Text(
                                  "Login",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      FlatButton(
                        onPressed: () {
                          Route route =
                              MaterialPageRoute(builder: (context) => Signup());
                          Navigator.push(context, route);
                        },
                        child: Text(
                          "Create Account",
                          style: TextStyle(
                              color: maincolor, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ))
            ],
          ),
        ));
  }

  Future<void> nextPage() async {
    SharedPreferences getSP = await SharedPreferences.getInstance();
    var email = getSP.getString('email');
    if (email != null) {
      Route route = MaterialPageRoute(builder: (context) => HomePage());
      Navigator.push(context, route);
    }
  }

  Future<void> signIn() async {
    setState(() {
      _isloading = true;
    });
    SharedPreferences getSP = await SharedPreferences.getInstance();
    var email = getSP.getString('email');
    _formKey.currentState.save();
    await Provider.of<Auth>(context, listen: false)
        .SignIn(_email, _password)
        .catchError((value) {
      setState(() {
        _isloading = false;
      });
    });
  }

}
