import 'package:flutter/material.dart';
import 'package:myshop/provider/cart.dart';
import 'package:provider/provider.dart';

import 'cartDetails.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  void initState() {
    // TODO: implement initState
    Future.delayed(Duration(milliseconds: 100), () {
      Provider.of<Cart>(context, listen: false)
          .fetchandretirve()
          .catchError((error) {
        print(error);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final listData = Provider.of<Cart>(context);
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Card(
              margin: EdgeInsets.all(15),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Total",
                      style: TextStyle(fontSize: 20),
                    ),
                    Spacer(),
                    Chip(
                      label: Text(
                        listData.totalAmount.toStringAsFixed(2),
                        style: TextStyle(
                            color:
                                Theme.of(context).primaryTextTheme.title.color),
                      ),
                      backgroundColor: Theme.of(context).primaryColor,
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            listData.MyCarItem.isEmpty
                ? Center(
                    child: Text(
                      "You have no cart item",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  )
                : Expanded(
                    child: ListView.builder(
                        itemCount: listData.MyCarItem.length,
                        itemBuilder: (_, int) {
                          return Dismissible(
                            key: ValueKey(listData.MyCarItem[int]),
                            background: Container(
                              color: Theme.of(context).errorColor,
                              child: Icon(
                                Icons.delete,
                                color: Colors.white,
                                size: 40,
                              ),
                              alignment: Alignment.centerRight,
                              padding: EdgeInsets.only(right: 20),
                              margin: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 4),
                            ),
                            direction: DismissDirection.endToStart,
                            confirmDismiss: (direction) {
                              return showDialog(
                                  context: context,
                                  builder: (ctx) => AlertDialog(
                                        title: Text("Are you Sure?"),
                                        content: Text(
                                            "Do you want remove the item from the cart?"),
                                        actions: <Widget>[
                                          FlatButton(
                                              onPressed: () {
                                                Navigator.of(ctx).pop(false);
                                              },
                                              child: Text("No")),
                                          FlatButton(
                                              onPressed: () {
                                                Navigator.of(ctx).pop(true);
                                              },
                                              child: Text("Yes")),
                                        ],
                                      ));
                            },
                            onDismissed: (direction) {
                              Provider.of<Cart>(context, listen: false)
                                  .removeList(int, listData.MyCarItem[int].id);
                            },
                            child: Card(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 4),
                              child: Padding(
                                padding: EdgeInsets.all(8),
                                child: ListTile(
                                  leading: CircleAvatar(
                                    child: Padding(
                                        padding: EdgeInsets.all(5),
                                        child: FittedBox(
                                            child: Text(
                                                "\$${listData.MyCarItem[int].price}"))),
                                  ),
                                  title: Text(listData.MyCarItem[int].title),
                                  subtitle: Text(
                                      "Total : \$${(listData.MyCarItem[int].price * listData.MyCarItem[int].quantity)}"),
                                  trailing: Text(
                                      "${listData.MyCarItem[int].quantity} x"),
                                  onTap: () {
                                    Route route = MaterialPageRoute(
                                        builder: (context) => CartDetails(
                                            listData.MyCarItem[int].id,
                                            listData.MyCarItem[int].imageUrl,
                                            listData.MyCarItem[int].colors,
                                            int.toString(),
                                            listData.MyCarItem[int].price,
                                            listData.MyCarItem[int].quantity,
                                            listData.MyCarItem[int].title));
                                    Navigator.push(context, route);
                                  },
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
          ],
        ),
      ),
    );
  }
}
