import 'package:flutter/material.dart';
import 'package:myshop/provider/cart.dart' as cart;
import 'package:myshop/provider/order.dart';
import 'package:provider/provider.dart';

class CartDetails extends StatefulWidget {
  final String id;
  final String imageUrl;
  final List colors;
  final String productID;
  final double price;
  final int quantity;
  final String title;

  CartDetails(this.id, this.imageUrl, this.colors, this.productID, this.price,
      this.quantity, this.title);

  @override
  _CartDetailsState createState() => _CartDetailsState();
}

class _CartDetailsState extends State<CartDetails> {
  int selectedRadio = 0;
  int GroupValue = 1;
  int QunAmount = 1;

//  double Total=0.0;

  void add() {
    setState(() {
      QunAmount++;
    });
  }

  void remove() {
    if (QunAmount != 1) {
      setState(() {
        QunAmount--;
      });
    }
  }

  double total = 0.0;

  void TotalAmount() {
    setState(() {
      total = QunAmount * widget.price;
    });
  }

  @override
  Widget build(BuildContext context) {
    TotalAmount();
    final Cart = Provider.of<cart.Cart>(context, listen: false);
    final order = Provider.of<Order>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          "Your Product",
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 200,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(widget.imageUrl), fit: BoxFit.cover)),
          ),
          Text(
            widget.title,
            style: TextStyle(fontSize: 25),
          ),
          Text(
            "\$${widget.price}",
            style: TextStyle(fontSize: 20, color: Colors.grey),
          ),
          Divider(),
          widget.colors == null
              ? Text(
                  "No Colors",
                  style: TextStyle(fontSize: 20),
                )
              : SizedBox(
                  width: double.infinity,
                  height: 40,
                  child: Center(
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: widget.colors.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, int) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                widget.colors[int],
                                style: TextStyle(fontSize: 20),
                              ),
                              Radio(
                                  value: int + 1,
                                  groupValue: GroupValue,
                                  onChanged: (value) {
                                    print(value);
                                    setState(() {
                                      GroupValue = value;
                                    });
                                  }),
                            ],
                          );
                        }),
                  ),
                ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(
                "Quantity :",
                style: TextStyle(fontSize: 20),
              ),
              RaisedButton(
                  onPressed: () {
                    remove();
                  },
                  child: Icon(Icons.remove)),
              Text(
                "$QunAmount",
                style: TextStyle(fontSize: 20),
              ),
              RaisedButton(
                  onPressed: () {
                    add();
                  },
                  child: Icon(Icons.add)),
            ],
          ),
          Spacer(),
          Text(
            "Your Total Product Price : ${total.toStringAsFixed(2)}",
            style: TextStyle(fontSize: 18),
          ),
          RaisedButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            color: Theme.of(context).primaryColor,
            onPressed: () {
              order
                  .addOrder(widget.imageUrl, total.toString(),
                  QunAmount.toString(), widget.colors[GroupValue - 1])
                  .then((value) {
                showDialog(
                    context: context,
                    builder: (ctx) => AlertDialog(
                      title: Text("Order is pending!"),
                      content: Text("We will contact with you later"),
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text("Okay"))
                      ],
                    ));
                Cart.removeList(int.parse(widget.productID), widget.id);
              });
            },
            child: Text(
              "Order Placed",
              style: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(
            height: 40,
          )
        ],
      ),
    );
  }
}
