import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:myshop/provider/cart.dart';
import 'package:myshop/widget/productitem.dart';
import 'package:myshop/provider/auth.dart';
import 'package:myshop/provider/product_provider.dart';
import 'package:myshop/screen/signin.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:http/http.dart' as http;
import 'package:myshop/widget/badge.dart';

import 'cartScreen.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({Key key, this.user}) : super(key: key);
  final FirebaseUser user;

  @override
  _DashBoardState createState() => _DashBoardState(this.user);
}

class _DashBoardState extends State<DashBoard> {
  List<Item> users = <Item>[
    const Item(
      'Fashions',
    ),
    Item(
      'Electronics',
    ),
  ];
  var Catrogy = "c1";

  Future SignOut() async {
    SharedPreferences getSp = await SharedPreferences.getInstance();
//    print(getSp.get("email"));

    await getSp.clear();
    Route route = MaterialPageRoute(builder: (context) => SignIn());
    Navigator.push(context, route);
  }

  Future getSlider() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection('Slider').getDocuments();
//    print(qn.documents);
    return qn.documents;
  }

  Item selectedUser;
  final FirebaseUser user;
  CarouselSlider carouselSlider;

  _DashBoardState(this.user);

  Color maincolor = Color.fromRGBO(1, 117, 216, 1.0);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<ProductProvider>(context, listen: false)
        .fetchAndSetProduct(context);
    setState(() {
      selectedUser = users.first;
    });
    getSlider();
  }

  @override
  Widget build(BuildContext context) {
    final productdata = Provider.of<ProductProvider>(context);

    final productItem = productdata.Category(Catrogy);

    int intSlider = 0;
    print(productItem.length);
    List Slider = [
      "https://image.shutterstock.com/image-photo/cheerful-happy-woman-enjoying-shopping-260nw-1417347668.jpg",
      "https://cdn.pixabay.com/photo/2017/12/26/09/15/woman-3040029_960_720.jpg",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSJO88oIBjchQJFCAHCfGjJRwGUH44jggpAdyY2oDfq7RlEkp3f&usqp=CAU"
    ];
    Provider.of<Cart>(context, listen: false).fetchandretirve();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actionsIconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          Consumer<Cart>(
            builder: (_, cartData, ch) =>
                Badge(child: ch, value: cartData.itemCount.toString()),
            child:
                IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {}),
          ),
        ],
        centerTitle: true,
        backgroundColor: Colors.white,
        title: DropdownButtonHideUnderline(
          child: DropdownButton<Item>(
            icon: Icon(
              // Add this
              Icons.arrow_drop_down, // Add this
              color: Colors.black, // Add this
            ),
            hint: Text(
              "Select item",
              style: TextStyle(color: Colors.black),
            ),
            value: selectedUser,
            onChanged: (Value) {
              if (Value.name == "Fashions") {
                setState(() {
                  Catrogy = "c1";
                });
              }
              if (Value.name == "Electronics") {
                setState(() {
                  Catrogy = "c2";
                });
              }
              setState(() {
                selectedUser = Value;
              });
            },
            items: users.map((Item user) {
              return DropdownMenuItem<Item>(
                value: user,
                child: Row(
                  children: <Widget>[
                    Text(
                      user.name,
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              );
            }).toList(),
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.28,
            width: double.infinity,
            child: carouselSlider = CarouselSlider(
              autoPlay: true,
              onPageChanged: (value) {
//                setState(() {
//                  intSlider = value;
//                });
              },
              viewportFraction: 1.0,
              aspectRatio: MediaQuery.of(context).size.aspectRatio,
              items: Slider.map(
                (url) {
                  return GestureDetector(
                    onTap: () {},
                    child: Stack(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.cover, image: NetworkImage(url))),
                        ),
                        Positioned(
                            bottom: 0,
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.35,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.bottomCenter,
                                      end: Alignment.topCenter,
                                      colors: [
                                    Colors.white,
                                    Colors.white24,
                                    Colors.white12
                                  ])),
                            )),
                      ],
                    ),
                  );
                },
              ).toList(),
            ),
          ),
          SizedBox(
            height: 05,
          ),
          Column(
            children: <Widget>[
              GridView.builder(
                  shrinkWrap: true,
                  itemCount: productItem.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    childAspectRatio: 3 / 3,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                  ),
                  itemBuilder: (_, int) => ChangeNotifierProvider.value(
                        value: productItem[int],
                        child: ProductItem(),
                      ))
            ],
          )
        ],
      ),
    );
  }
}

class Item {
  const Item(this.name);

  final String name;
//  final Icon icon;
}
