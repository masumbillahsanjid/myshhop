import 'package:flutter/material.dart';
import 'package:myshop/provider/cart.dart';
import 'package:myshop/screen/signin.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Profile extends StatelessWidget {
  Future SignOut(BuildContext context) async {
    SharedPreferences getSp = await SharedPreferences.getInstance();
//    print(getSp.get("email"));
    Provider.of<Cart>(context, listen: false).clear();
    await getSp.clear();
    Route route = MaterialPageRoute(builder: (context) => SignIn());
    Navigator.push(context, route);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: RaisedButton(
              onPressed: () {
                SignOut(context);
              },
              child: Text("Log Out"),
            ),
          )
        ],
      ),
    );
  }
}
