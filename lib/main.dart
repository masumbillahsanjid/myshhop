import 'package:flutter/material.dart';
import 'package:myshop/provider/auth.dart';
import 'package:myshop/provider/cart.dart';
import 'package:myshop/provider/order.dart';
import 'package:myshop/provider/product_provider.dart';
import 'package:myshop/screen/signin.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'HomePage.dart';
import 'screen/dashboard.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences getSP = await SharedPreferences.getInstance();
  var email = getSP.getString('email');
  print(email);
  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Auth()),
        ChangeNotifierProvider.value(value: ProductProvider()),
        ChangeNotifierProvider.value(value: Cart()),
        ChangeNotifierProvider.value(value: Order()),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: email == null ? SignIn() : HomePage())));
}
